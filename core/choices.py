#-*- coding: utf-8 -*-

FIELD_TYPES = {
    0: 'integer',
    1: 'text',
    2: 'bool',
    3: 'textarea',
}

FIELD_TYPES_BY_NAME = {
    'integer': 0,
    'text': 1,
    'bool': 2,
    'textarea': 3,
}