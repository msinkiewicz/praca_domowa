#-*- coding: utf-8 -*-
from django.db import models


class AnnouncementCategory(models.Model):
    name = models.CharField(max_length=20, unique=True)
    customfield_definition = models.TextField(null=True)


class Announcement(models.Model):
    '''
    Announcement database model
    @ivar category: announcement category
    @ivar title: announcement title
    @ivar slug: announcement slug created from title
    @ivar additional_data:additional data from customfields
    '''
    
    category = models.ForeignKey(AnnouncementCategory, related_name='announcements')
    title = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)
    date_added = models.DateTimeField(auto_now_add=True)
    additional_data = models.TextField()
