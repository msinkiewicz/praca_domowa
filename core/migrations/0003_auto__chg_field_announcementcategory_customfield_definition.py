# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'AnnouncementCategory.customfield_definition'
        db.alter_column('core_announcementcategory', 'customfield_definition', self.gf('django.db.models.fields.TextField')(null=True))

    def backwards(self, orm):

        # Changing field 'AnnouncementCategory.customfield_definition'
        db.alter_column('core_announcementcategory', 'customfield_definition', self.gf('django.db.models.fields.TextField')(default=[]))

    models = {
        'core.announcement': {
            'Meta': {'object_name': 'Announcement'},
            'additional_data': ('django.db.models.fields.TextField', [], {}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.AnnouncementCategory']"}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'core.announcementcategory': {
            'Meta': {'object_name': 'AnnouncementCategory'},
            'customfield_definition': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'})
        }
    }

    complete_apps = ['core']