# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AnnouncementCategory'
        db.create_table('core_announcementcategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
            ('customfield_definition', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('core', ['AnnouncementCategory'])

        # Adding field 'Announcement.date_added'
        db.add_column('core_announcement', 'date_added',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2012, 9, 18, 0, 0), blank=True),
                      keep_default=False)


        # Renaming column for 'Announcement.category' to match new field type.
        db.rename_column('core_announcement', 'category', 'category_id')
        # Changing field 'Announcement.category'
        db.alter_column('core_announcement', 'category_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.AnnouncementCategory']))
        # Adding index on 'Announcement', fields ['category']
        db.create_index('core_announcement', ['category_id'])


    def backwards(self, orm):
        # Removing index on 'Announcement', fields ['category']
        db.delete_index('core_announcement', ['category_id'])

        # Deleting model 'AnnouncementCategory'
        db.delete_table('core_announcementcategory')

        # Deleting field 'Announcement.date_added'
        db.delete_column('core_announcement', 'date_added')


        # Renaming column for 'Announcement.category' to match new field type.
        db.rename_column('core_announcement', 'category_id', 'category')
        # Changing field 'Announcement.category'
        db.alter_column('core_announcement', 'category', self.gf('django.db.models.fields.IntegerField')())

    models = {
        'core.announcement': {
            'Meta': {'object_name': 'Announcement'},
            'additional_data': ('django.db.models.fields.TextField', [], {}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.AnnouncementCategory']"}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'core.announcementcategory': {
            'Meta': {'object_name': 'AnnouncementCategory'},
            'customfield_definition': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'})
        }
    }

    complete_apps = ['core']