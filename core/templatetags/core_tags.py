#-*- coding: utf-8 -*-
from django import template
from core import choices
import trans
import simplejson

register = template.Library()


def slugify(text, replace='-', remove_white_signs=False):
    '''Exchanges polish letters'''
    polish_alphabet = u'ąćęłńóśżź'
    slugified_alphabet = u'acelnoszz'
    # dodatkowe znaki specjalne i ich zastępstwa
    substitute_letters = {
        '|': '',
        ',': '',
    }
    text = text.lower()
    # zamiana spacji na znak zastępczy
    text = text.replace(' ', replace)
    output = []
    # zamiana polskich liter na odpowiedniki
    for index in xrange(0, len(text)):
        try:
            if text[index] in polish_alphabet:
                output.append(slugified_alphabet[polish_alphabet.find(text[index])])
            else:
                output.append(text[index])
        except:
            continue
    # zamiana zdefiniowanych znaków specjalnych
    if remove_white_signs:
        for index in range(0, len(output)):
            if substitute_letters.get(output[index], None):
                del output[index]
    return u''.join(output)

def extract_content(announcement):
    '''gets announcement data and returns it in html form'''
    additional_data = simplejson.loads(announcement.additional_data)
    if not additional_data:
        return ''
    category = announcement.category
    if not category:
        return ''
    field_definitions = simplejson.loads(category.customfield_definition).get('fields')
    try:
        output = []
        for key, value in additional_data.items():
            if not value:
                continue
            if field_definitions.get(key, {}):
                output.append(u'<span><strong>{0}:</strong> {1}</span>'.format(trans.TRANS_DICT.get(field_definitions.get(key).get('label'), field_definitions.get(key).get('label')), value))
        return ', '.join(output)
    except ValueError:
        pass
    return additional_data

def check_start_row(counter, per_row=3):
    print counter, per_row, counter%per_row
    if counter%per_row == 0:
        return True
    return False

def check_end_row(counter, per_row=3):
    print counter, per_row, counter%per_row
    if counter%per_row == per_row - 1:
        return True
    return False

# rejestrowanie filtrów/tagów
register.filter('slugify', slugify)
register.filter('extract_content', extract_content)
register.filter('check_start_row', check_start_row)
register.filter('check_end_row', check_end_row)
