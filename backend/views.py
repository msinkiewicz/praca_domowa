#-*- coding: utf-8 -*-
from django.views.generic.base import TemplateView, View
from core import models
from django.views.generic.edit import FormView
from backend.forms import AnnouncementForm, AnnouncementCategoryForm, \
    CustomFieldForm
import simplejson
from core.templatetags.core_tags import slugify
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.forms.formsets import formset_factory
import datetime
from hashlib import md5
from django.shortcuts import redirect


class MainView(TemplateView):
    '''Main view'''
    template_name = 'backend/index.html'

    def get_context_data(self, *args, **kwargs):
        '''creates context for template'''
        context = super(MainView, self).get_context_data(*args, **kwargs)
        return context


class CategoryEditView(FormView):
    form_class = AnnouncementCategoryForm
    template_name = 'backend/category.html'
    category = None

    def get_success_url(self):
        return reverse('category-edit', kwargs={'category_id': self.get_queryset().id})

    def get_queryset(self):
        if not self.category:
            try:
                self.category = models.AnnouncementCategory.objects.get(id=self.kwargs.get('category_id'))
            except ObjectDoesNotExist:
                pass
        return self.category

    def get_customfield_formset(self, post=None, initial=False):
        formset = formset_factory(CustomFieldForm)
        if initial:
            category = self.get_queryset()
            data = []
            if category and category.customfield_definition:
                
                for key, value in simplejson.loads(category.customfield_definition).get('fields', {}).items():
                    print key, value
                    dict_ = {}
                    dict_['field_id'] = key
                    for data_key, data_value in value.items():
                        dict_[data_key] = data_value
                    print dict_
                    data.append(dict_)
            return formset(initial=data, prefix='customfield')
        elif post:
            return formset(data=post, prefix='customfield')

    def get_initial(self):
        output = {}
        if self.get_queryset():
            output['name'] = self.get_queryset().name
            output['category_id'] = self.get_queryset().id
        return output

    def get_context_data(self, *args, **kwargs):
        '''creates context for template - gets announcements for category'''
        context = super(CategoryEditView, self).get_context_data(*args, **kwargs)
        context['announcements'] = models.Announcement.objects.filter(category=self.kwargs.get('category_id'))
        context['category'] = self.get_queryset()
        context['customfield_formset'] = self.get_customfield_formset(initial=True)
        return context

    def form_valid(self, form):
        if self.get_queryset():
            category = self.get_queryset()
        else:
            category = models.AnnouncementCategory.objects.create(name=form.cleaned_data.get('name'))
        customfield_data = {}
        for customfield_form in self.get_customfield_formset(post=self.request.POST):
            if customfield_form.is_valid():
                data_ = {}
                for key, value in customfield_form.cleaned_data.items():
                    data_[key] = value
                if customfield_form.cleaned_data.get('field_id'):
                    data_id = customfield_form.cleaned_data.get('field_id')
                else:
                    data_id =  md5(datetime.datetime.now().__str__()).hexdigest()
                data_['field_id'] = data_id
                customfield_data[data_id] = data_
        customfield_definition = simplejson.loads(category.customfield_definition) if category.customfield_definition else {}
        customfield_definition['fields'] = customfield_data
        category.customfield_definition = simplejson.dumps(customfield_definition)
        category.save()
        self.category = category
        return super(CategoryEditView, self).form_valid(form)

class AnnouncementEditView(FormView):
    '''Add/Edit announcement view'''
    form_class = AnnouncementForm
    template_name = 'backend/announcement_edit.html'
    announcement = None

    def get_queryset(self):
        '''Returns announcement object based on id provided in view kwargs or None if it's used for creating a new announcement'''
        if self.announcement:
            return self.announcement
        if self.kwargs.get('announcement_id'):
            try:
                self.announcement = models.Announcement.objects.get(id=self.kwargs.get('announcement_id'))
            except ObjectDoesNotExist:
                pass
        return self.announcement

    def get_success_url(self):
        '''Success callback function - returns url for edit announcement view'''
        return reverse('announcement-edit', kwargs={'category_id':self.get_queryset().category.id,
                                                            'announcement_id': self.get_queryset().id})

    def get_initial(self):
        '''Returns initial data for form'''
        data_ = {}
        announcement = self.get_queryset()
        if announcement:
            # announcement category
            data_['category_id'] = announcement.category.id
            # announcement title
            data_['title'] = announcement.title
            # announcement data from customfields
            if announcement.additional_data:
                try:
                    for key, value in simplejson.loads(announcement.additional_data).items():
                        # polish signs fix
                        data_[slugify(key)] = value
                except Exception, e:
                    print e
        else:
            try:
                data_['category_id'] = self.kwargs.get('category_id')
            except ObjectDoesNotExist:
                raise Exception('Category Does Not Exist')
        return data_

    def get_context_data(self, *args, **kwargs):
        '''creates context for template'''
        context = super(AnnouncementEditView, self).get_context_data(*args, **kwargs)
        return context

    def form_valid(self, form):
        '''
        Callback function fired atfter successful form with customfields validation.
        Creates or updates announcement data.
        '''
        category = models.AnnouncementCategory.objects.get(id=form.cleaned_data.get('category_id'))
        if not self.get_queryset():
            announcement = models.Announcement.objects.create(title=form.cleaned_data.get('title'),
                                                          slug=slugify(form.cleaned_data.get('title')),
                                                          category=category)
        else:
            announcement = self.get_queryset()
            announcement.title = form.cleaned_data.get('title')
            announcement.slug = slugify(form.cleaned_data.get('title'))
        # customfields data serialising and saving
        additional_data = {}
        for key in simplejson.loads(category.customfield_definition).get('fields', {}).keys():
            additional_data[key] = form.cleaned_data.get(slugify(key))
        announcement.additional_data = simplejson.dumps(additional_data)
        announcement.save()
        # tying announcement to view for returning success_url purpose (new announcements)
        if not self.announcement:
            self.announcement = announcement
        return super(AnnouncementEditView, self).form_valid(form)
            

class FieldDeleteView(View):
    def get(self, request, *args, **kwargs):
        try:
            category = models.AnnouncementCategory.objects.get(id=kwargs.get('category_id'))
            fields_def = simplejson.loads(category.customfield_definition)
            if fields_def.get('fields',{}).get(kwargs.get('field_id')):
                del fields_def['fields'][kwargs.get('field_id')]
                category.customfield_definition = simplejson.dumps(fields_def)
                category.save()
        except ObjectDoesNotExist:
            pass
        return redirect(request.META['HTTP_REFERER'])

