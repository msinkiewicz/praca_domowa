#-*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from backend.views import MainView, AnnouncementEditView,\
    CategoryEditView, FieldDeleteView


urlpatterns = patterns('praca_domowa.backend.urls',
    url(r'^category/add/$', CategoryEditView.as_view(), name='category-add'),
    url(r'^category/(?P<category_id>\d+)/$', CategoryEditView.as_view(), name='category-edit'),
    
    url(r'^delete-field/(?P<category_id>\d+)/(?P<field_id>\w+)/$', FieldDeleteView.as_view(), name='field-delete'),
    
    # add/edit announcements from category
    url(r'^category/(?P<category_id>\d+)/add/', AnnouncementEditView.as_view(), name='announcement-add'),
    url(r'^category/(?P<category_id>\d+)/edit/(?P<announcement_id>\d+)/', AnnouncementEditView.as_view(), name='announcement-edit'),
    
    url(r'^', MainView.as_view(), name='backend-main'),
)
