#-*- coding: utf-8 -*-
from django import forms
from django.forms.util import ErrorList
from django.utils.translation import ugettext_lazy as _

from core import choices
from core.models import AnnouncementCategory
from core.templatetags.core_tags import slugify

import trans, simplejson
 

class AnnouncementCategoryForm(forms.Form):
    '''Category form'''
    name = forms.CharField(max_length=20)
    category_id = forms.CharField(widget=forms.HiddenInput, required=False)


class CustomFieldForm(forms.Form):
    '''Category Customfield form'''
    label = forms.CharField(max_length=20, required=True, label=_(u'Nazwa'))
    type = forms.ChoiceField(choices=choices.FIELD_TYPES.items(), label=_(u'Typ danych'))
    min = forms.IntegerField(required=False, label=_(u'Minimalna wartość'))
    max = forms.IntegerField(required=False, label=_(u'Maksymalna wartość'))
    max_length = forms.IntegerField(required=False, label=_(u'Maksymalna długość'))
    required = forms.BooleanField(required=False, label=_(u'Wymagane'))
    field_id = forms.CharField(required=False, widget=forms.HiddenInput)

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix=':',
                 empty_permitted=False):
        super(CustomFieldForm, self).__init__(data=data, files=files, auto_id=auto_id, prefix=prefix,
                 initial=initial, error_class=error_class, label_suffix=label_suffix,
                 empty_permitted=empty_permitted)
        if initial:
            field_type = int(initial.get('type'))
            if field_type == choices.FIELD_TYPES_BY_NAME.get('integer'):
                self.fields['max_length'].widget = forms.HiddenInput()
            elif field_type == choices.FIELD_TYPES_BY_NAME.get('text'):
                self.fields['max'].widget = forms.HiddenInput()
                self.fields['min'].widget = forms.HiddenInput()
            elif field_type == choices.FIELD_TYPES_BY_NAME.get('bool'):
                self.fields['max'].widget = forms.HiddenInput()
                self.fields['min'].widget = forms.HiddenInput()
                self.fields['max_length'].widget = forms.HiddenInput()
                self.fields['required'].widget = forms.HiddenInput()
            elif field_type == choices.FIELD_TYPES_BY_NAME.get('textarea'):
                self.fields['max'].widget = forms.HiddenInput()
                self.fields['min'].widget = forms.HiddenInput()
                self.fields['max_length'].widget = forms.HiddenInput()
        if not initial and not data:
            self.fields['max'].widget = forms.HiddenInput()
            self.fields['min'].widget = forms.HiddenInput()
            self.fields['max_length'].widget = forms.HiddenInput()
            self.fields['required'].widget = forms.HiddenInput()


class AnnouncementForm(forms.Form):
    '''Announcement form'''
    title = forms.CharField(max_length=255, required=True, label=_(u'Tytuł'))
    category_id = forms.CharField(widget=forms.HiddenInput, required=True)

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix=':',
                 empty_permitted=False):
        '''
        Constructor - it creates default fields and additional fields
        defined for the category
        '''
        super(AnnouncementForm, self).__init__(data=data, files=files, auto_id=auto_id, prefix=prefix,
                 initial=initial, error_class=error_class, label_suffix=label_suffix,
                 empty_permitted=empty_permitted)
        if initial:
            # gathering data for the additional fields defined in the category and creating formfield
            # based on their type including parameters such as required, max, min, max_length, label
            customfield_dict = simplejson.loads(AnnouncementCategory.objects.get(id=initial.get('category_id', 0)).customfield_definition).get('fields', {})
            for key, value in customfield_dict.items():
                if choices.FIELD_TYPES.get(int(value.get('type'))) == 'integer':
                    self.fields[slugify(key)] = forms.IntegerField(max_value=value.get('max', None),
                                                          min_value=value.get('min', None),
                                                          required=value.get('required', False),
                                                          label=trans.TRANS_DICT.get(value.get('label'), value.get('label')))
                elif choices.FIELD_TYPES.get(int(value.get('type'))) == 'text':
                    self.fields[slugify(key)] = forms.CharField(max_length=value.get('max_length', None),
                                                       required=value.get('required', False),
                                                       label=trans.TRANS_DICT.get(value.get('label'), value.get('label')))
                elif choices.FIELD_TYPES.get(int(value.get('type'))) == 'textarea':
                    self.fields[slugify(key)] = forms.CharField(required=value.get('required', False),
                                                       label=trans.TRANS_DICT.get(value.get('label'), value.get('label')),
                                                       widget=forms.Textarea)
                elif choices.FIELD_TYPES.get(int(value.get('type'))) == 'bool':
                    self.fields[slugify(key)] = forms.BooleanField(label=trans.TRANS_DICT.get(key, key), widget=forms.CheckboxInput, required=False)
