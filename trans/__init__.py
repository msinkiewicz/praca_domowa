#-*- coding: utf-8 -*-

'''Dodanie tłumaczeń poszczególnych niestandardowych treści/labelek'''


TRANS_DICT = {
    u'legs': u'Nogi',
    u'breed': u'Rasa',
    u'doors': u'Drzwi',
    u'color': u'Kolor' 
}
