from django.core.context_processors import request
from core import models


def companies_processor(request):
    return {'categories': models.AnnouncementCategory.objects.all()}