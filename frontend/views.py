#-*- coding: utf-8 -*-
from django.views.generic.base import TemplateView
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from core import choices, models
from core.templatetags.core_tags import slugify
from backend.views import AnnouncementEditView


class MainView(TemplateView):
    '''Frontend main view'''
    template_name = 'frontend/index.html'

    def get_context_data(self, *args, **kwargs):
        '''Wrzucenie do kontekstu danych - kategorie wraz z 5 ostatnimi ogłoszeniami przypisanymi do nich'''
        context = super(MainView, self).get_context_data(*args, **kwargs)
        context['announcements'] = models.Announcement.objects.all()[:8]
        return context


class CategoryView(TemplateView):
    '''Category view - announcement list for selected category'''
    template_name = 'frontend/category.html'

    def get_context_data(self, *args, **kwargs):
        '''Tworzy kontekst dla szablonu - ogłoszenia dla konkretnej kategorii'''
        context = super(CategoryView, self).get_context_data(*args, **kwargs)
        context['announcements'] = models.Announcement.objects.filter(category=self.kwargs.get('category_id'))
        try:
            context['category'] = models.AnnouncementCategory.objects.get(id=self.kwargs.get('category_id'))
        except ObjectDoesNotExist:
            raise Http404("Kategoria nie istnieje")
        return context


class AnnouncementEditView(AnnouncementEditView):
    '''Announcement Edit view - inherits from backend's edit view, overrides template and success_url'''
    template_name = 'frontend/announcement_edit.html'

    def get_context_data(self, *args, **kwargs):
        '''Creates context for template'''
        context = super(AnnouncementEditView, self).get_context_data(*args, **kwargs)
        try:
            context['category'] = models.AnnouncementCategory.objects.get(id=self.kwargs.get('category_id'))
        except ObjectDoesNotExist:
            raise Http404(_(u"Kategoria nie istnieje"))
        return context

    def get_success_url(self):
        category = self.announcement.category
        return reverse('frontend-category', kwargs={'category_id': category.id,'category_slug': slugify(category.name)})

