#-*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from frontend.views import MainView, CategoryView, AnnouncementEditView


urlpatterns = patterns('praca_domowa.frontend.urls',
    # Main page views
    url(r'^c/(?P<category_id>\d+),(?P<category_slug>[\s\S]+)/add/', AnnouncementEditView.as_view(), name='frontend-announcement-add'),
    url(r'^c/(?P<category_id>\d+),(?P<category_slug>[\s\S]+)/$', CategoryView.as_view(), name='frontend-category'),
    url(r'^', MainView.as_view(), name='frontend-main'),
)
